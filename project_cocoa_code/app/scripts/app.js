/*
* Created by: Philippe De Pauw - Waterschoot
* Date: 19-11-2014
* Name: app.js
* Description: Pets application
* Dierenartsen: http://data.appsforghent.be/poi/dierenartsen.json
* Hondentoiletten: http://data.appsforghent.be/poi/hondentoiletten.json
* Hondenvoorzieningen: http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json
*
*/
(function(){
    'use strict';

    var App = {
        init:function(){
            //Create the API's via a clone
            this.dierenartsenAPI = DierenartsenAPI;
            this.dierenartsenAPI.init('http://data.appsforghent.be/poi/dierenartsen.json');//Initialize
            this.hondentoilettenAPI = HondentoilettenAPI;
            this.hondentoilettenAPI.init('http://data.appsforghent.be/poi/hondentoiletten.json');//Initialize
            this.hondenvoorzieningenAPI = HondenvoorzieningenAPI;
            this.hondenvoorzieningenAPI.init('http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json');//Initialize
            //Create PetsDBContext object via a clone
            this.petsDBContext = PetsDBContext;
            this.petsDBContext.init('dds.ghent.pets');//Initialize
            //Create Handlebars Cache for templates
            this.hbsCache = {};
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Geolocation
            this.geoLocation = null;

            //Check Google Maps Initialized
            this.gMap = null;
            this.checkGoogleMapsInitialized();

            //Get GEOLocation
            if(this.geoLocation === null){
                this.getGEOLocation();
            }

            //Get Vets
            if(this.petsDBContext.getVets() === null){
                this.getVetsFromApi();
            }
            //Get Dogs Toilets
            if(this.petsDBContext.getDogsToilets() === null){
                this.getDogsToiletsFromApi();
            }
            //Get Dogs Running Places
            if(this.petsDBContext.getDogsRunnigPlaces() === null){
                this.getDogsRunningPlacesFromApi();
            }

            //Render the interface
            this.render();
        },
        checkGoogleMapsInitialized:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            if(!window.googleMapsInitialized){
                window.setTimeout(function(){self.checkGoogleMapsInitialized();}, 1000);
            }else{
                this.gMap = GMap;//Clone
                this.gMap.init('gmap-canvas');
            }
        },
        cacheElements:function(){
            /* Cache Handlebars Templates
            * Vets, ...
            */
            var src = null;
            if(!this.hbsCache['vets']){
                src = document.querySelector('#vets-template').innerHTML;
                this.hbsCache['vets'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dogsrunningplaces']){
                src = document.querySelector('#dogsrunningplaces-template').innerHTML;
                this.hbsCache['dogsrunningplaces'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dogstoilets']){
                src = document.querySelector('#dogstoilets-template').innerHTML;
                this.hbsCache['dogstoilets'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['nearestplaces']){
                src = document.querySelector('#nearestplaces-template').innerHTML;
                this.hbsCache['nearestplaces'] = Handlebars.compile(src);
            }
            /* DOM Elements
            * Vets List, ...
            */
            this.offcanvasNavigation = document.querySelector('.offcanvas-navigation');
            this.offcanvasToggles = document.querySelectorAll(".offcanvas-toggle");
            this.vetsList =  document.querySelector('#vets-list');
            this.dogsrunningplacesList =  document.querySelector('#runningplaces-list');
            this.dogstoiletsList =  document.querySelector('#toilets-list');
            this.nearestList = document.querySelector('#nearest-list');
        },
        bindEvents:function(){

            _.each(this.offcanvasToggles, function(toggle){
                toggle.addEventListener("click", function(ev){
                    ev.preventDefault();

                    document.body.classList.toggle('offcanvasactive');

                    return false;
                }, false);
            });
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('home');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('home');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);document.body.classList.remove('offcanvasactive');});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        setActivePage:function(section){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages !== null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id === section){
                        page.classList.add('active');
                        var pageStatesNavigation = page.querySelector('.page-state-navigation');

                        if(pageStatesNavigation){
                            var as = pageStatesNavigation.querySelectorAll('ul>li>a');
                            _.each(as, function(a){
                                a.addEventListener('click', function(ev){
                                    ev.preventDefault();

                                    self.setActivePageState(this.parentNode.dataset.page, this.parentNode.dataset.state);

                                    return false;
                                });
                            });
                        }
                        //Move Google Maps to the page
                        var connector = page.querySelector('.gmap-connector');
                        if(connector !== null){
                            connector.appendChild(document.querySelector('#gmap-canvas'));
                            if(self.gMap !== null && typeof self.gMap !== 'undefined'){
                                self.gMap.showMarkersForPage(section);
                                self.gMap.refresh();
                            }
                        }


                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.offcanvas-navigation ul li a');
            if(navLinks !== null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') === effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        },
        setActivePageState:function(page, state){
            var pageStates = document.querySelectorAll('#' + page + ' .page-state');
            if(pageStates !== null && pageStates.length > 0){
                _.each(pageStates,function(pageState){
                    if(pageState.dataset.state === state){
                        pageState.classList.add('active');
                    }else{
                        pageState.classList.remove('active');
                    }
                });
            }

            var pageStatesNavigation = document.querySelectorAll('[data-page=' + page + ']');
            if(pageStatesNavigation !== null && pageStatesNavigation.length > 0){
                _.each(pageStatesNavigation,function(pageStateNav){
                    if(pageStateNav.dataset.state === state){
                        pageStateNav.classList.add('active');
                    }else{
                        pageStateNav.classList.remove('active');
                    }
                });
            }
        },
        render:function(){
            //Render the vets
            this.renderVets(this.petsDBContext.getVets());
            //Render the dogs running places
            this.renderDogsRunningPlaces(this.petsDBContext.getDogsRunnigPlaces());
            //Render the dogs toilets
            this.renderDogsToilets(this.petsDBContext.getDogsToilets());
            //Render nearest places
            this.renderNearestPlaces(this.petsDBContext.getNearestPlaces(this.geoLocation));
        },
        renderVets:function(vets){
            if(vets !== null && this.vetsList !== null){
                vets.geoLocation = this.geoLocation;
                this.vetsList.innerHTML = this.hbsCache['vets'](vets);

                if(this.gMap !== null){
                    this.gMap.addMarkersForVets(vets.vets);
                }
            }
        },
        renderDogsRunningPlaces:function(places){
            if(places !== null && this.dogsrunningplacesList !== null){
                places.geoLocation = this.geoLocation;
                this.dogsrunningplacesList.innerHTML = this.hbsCache['dogsrunningplaces'](places);

                if(this.gMap !== null){
                    this.gMap.addMarkersForDogsRunningPlaces(places.dogsrunningplaces);
                }
            }
        },
        renderDogsToilets:function(toilets){
            if(toilets !== null && this.dogstoiletsList !== null){
                toilets.geoLocation = this.geoLocation;
                this.dogstoiletsList.innerHTML = this.hbsCache['dogstoilets'](toilets);

                if(this.gMap !== null){
                    this.gMap.addMarkersForDogsToilets(toilets.dogstoilets);
                }
            }
        },
        renderNearestPlaces:function(places){
            if(places !== null && this.nearestList !== null){
                this.nearestList.innerHTML = this.hbsCache['nearestplaces'](places);
            }
        },
        getVetsFromApi:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            this.dierenartsenAPI.getDierenartsen().then(
                function(data){
                    //Get the value of the key dierenartsen
                    var results = data.dierenartsen;
                    //Loop through the objects within the results --> Array
                    _.each(results, function(obj){
                        self.petsDBContext.addVet(obj);
                    });
                    //Render the vets
                    self.renderVets(self.petsDBContext.getVets());
                },
                function(result){
                    console.log(result);
                }
            );
        },
        getDogsRunningPlacesFromApi:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            this.hondenvoorzieningenAPI.getHondenvoorzieningen().then(
                function(data){
                    //Get the value of the key Hondenvoorzieningen
                    var results = data.Hondenvoorzieningen;
                    //Filter the results
                    results = _.filter(results, function(obj){
                        return obj.soort === 'Losloopweide';
                    });
                    //Loop through the objects within the results --> Array
                    _.each(results, function(obj){
                        self.petsDBContext.addDogsRunningPlace(obj);
                    });
                    //Render the dogs running places
                    self.renderDogsRunningPlaces(self.petsDBContext.getDogsRunnigPlaces());
                },
                function(result){
                    console.log(result);
                }
            );
        },
        getDogsToiletsFromApi:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            this.hondentoilettenAPI.getHondentoiletten().then(
                function(data){
                    //Get the value of the key hondentoiletten
                    var results = data.hondentoiletten;
                    console.log(results);
                    //Loop through the objects within the results --> Array
                    _.each(results, function(obj){
                        self.petsDBContext.addDogsToilet(obj);
                    });
                    //Render the dogs running places
                    self.renderDogsToilets(self.petsDBContext.getDogsToilets());
                },
                function(result){
                    console.log(result);
                }
            );
        },
        getGEOLocation:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            Utils.getGEOLocationByPromise().then(
                function(location){
                    self.geoLocation = location;
                    //Render the interface again
                    self.render();
                    //Add GeoLocation To The Map
                    if(self.gMap !== null){
                        self.gMap.addMarkerGeoLocation(self.geoLocation);
                    }
                },
                function(error){
                    self.geoLocation = null;
                }
            );
        }
    };

    App.init();//Initialize the application
})();